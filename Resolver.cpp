//
// Created by root on 12-May-20.
//

#include <numeric>
#include "Resolver.hpp"

bool Resolver::CheckLogin(const std::string &login) {
    for (auto&& c : login) {
        if (c < 'A')
            return false;
    }
    return true;
}

std::string Resolver::GeneratePassword(const std::string &login) {
    auto u_login = login;
    for (auto& c: u_login) {
        if (c > 'Z')
            c -= 0x20;
    }
    auto sum_hash = std::accumulate(begin(u_login), end(u_login), 0);
    return std::to_string(sum_hash ^ 0x5678 ^ 0x1234);
}
