//
// Created by root on 12-May-20.
//

#ifndef KEYGEN_RESOLVER_HPP
#define KEYGEN_RESOLVER_HPP


#include <string>

namespace Resolver {
    bool CheckLogin(const std::string& login);
    std::string GeneratePassword(const std::string& login);
};


#endif //KEYGEN_RESOLVER_HPP
