#include <iostream>
#include "TXLib.h"
#include "Resolver.hpp"
const size_t BUFFER_SIZE = 1024;

/**
 * Class for KeyGen form. Inspired with TxLib:6395
 */
class KeyGenForm : public txDialog{
public:
    KeyGenForm() : buffer_("") {};
    void Start();
    TX_BEGIN_MESSAGE_MAP() // Message Receive function setup
        TX_COMMAND_MAP
                TX_HANDLE(103)  GetDlgItemText(_wnd, 102, buffer_, BUFFER_SIZE - 1);
                                if (!Resolver::CheckLogin(buffer_)) {
                                    MessageBoxA(_wnd, "Invalid login", "Error", _result);
                                    SetDlgItemText(_wnd, 104, "");
                                } else {
                                    auto passwd = Resolver::GeneratePassword(buffer_);
                                    SetDlgItemText(_wnd, 104, passwd.c_str());
                                }

    TX_END_MESSAGE_MAP
private:
    char buffer_[BUFFER_SIZE];

    txDialog::Layout layout[7] = {
            {txDialog::DIALOG, "KEYGEN for CRACK_ME", 0, 0, 0, 200, 50},
            {txDialog::STATIC, "Login:",              101, 10, 12, 30, 10, SS_LEFT},
            {txDialog::STATIC, "Serial:",             105, 10, 32, 30, 10, SS_LEFT},
            {txDialog::EDIT,   "",                    102, 40, 10, 100, 15, WS_BORDER | ES_AUTOHSCROLL},
            {txDialog::BUTTON, "Generate!",           103, 150, 30, 40, 15, BS_DEFPUSHBUTTON},
            {txDialog::EDIT,   "",                    104, 40, 30, 100, 15, WS_BORDER | ES_AUTOHSCROLL | ES_READONLY},
            {txDialog::END}
    };
};

void KeyGenForm::Start() {
    dialogBox(layout);
}

int main() {
    txPlaySound("theme.wav", SND_LOOP);
    KeyGenForm form;
    form.Start();

    return 0;
}